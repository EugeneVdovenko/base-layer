variable project {
  description = "GCP project ID"
  type        = string
}

variable region {
  description = "GCP region"
  type        = string
  default     = "europe-north1"
}

variable name {
  description = "Environment name"
  type        = string
  default     = "develop"
}